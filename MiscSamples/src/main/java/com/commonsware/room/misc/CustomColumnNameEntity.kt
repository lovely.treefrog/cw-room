/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import android.database.Cursor
import androidx.room.*

@Entity(tableName = "customColumn")
class CustomColumnNameEntity(
  @PrimaryKey
  val id: String,
  val title: String,
  @ColumnInfo(name = "words") val text: String? = null,
  val version: Int = 1
) {
  @Dao
  interface Store {
    @Query("SELECT * FROM customColumn")
    fun loadAll(): List<CustomColumnNameEntity>

    @Query("SELECT * FROM customColumn where id = :id")
    fun findByPrimaryKey(id: String): CustomColumnNameEntity

    @Query("SELECT * FROM customColumn where id IN (:ids)")
    fun findByPrimaryKeys(vararg ids: String): List<CustomColumnNameEntity>

    @Query("SELECT * FROM customColumn where id IN (:ids)")
    fun findByPrimaryKeys(ids: List<String>): List<CustomColumnNameEntity>

    @Query("SELECT * FROM customColumn LIMIT :limit")
    fun loadFirst(limit: Int): List<CustomColumnNameEntity>

    @Query("SELECT * FROM customColumn")
    fun loadCursor(): Cursor

    @Insert
    fun insert(entity: CustomColumnNameEntity)
  }
}