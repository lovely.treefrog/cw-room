package com.commonsware.room.misc

import androidx.room.*

@Entity(tableName = "nullableProperty")
class NullablePropertyEntity(
  @PrimaryKey
  val id: String,
  val title: String,
  val text: String? = null,
  val version: Int = 1
) {
  @Dao
  interface Store {
    @Query("SELECT * FROM nullableProperty")
    fun loadAll(): List<NullablePropertyEntity>

    @Query("SELECT * FROM nullableProperty where id = :id")
    fun findByPrimaryKey(id: String): NullablePropertyEntity

    @Insert
    fun insert(entity: NullablePropertyEntity)
  }
}