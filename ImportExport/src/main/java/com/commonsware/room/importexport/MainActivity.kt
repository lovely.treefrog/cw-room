/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.importexport

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.observe
import com.commonsware.room.importexport.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

private val DATE_TIME_FORMATTER =
  DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
    .withZone(ZoneId.systemDefault())
private const val DEFAULT_EXPORT_TITLE = "ImportExport.db"
private const val REQUEST_EXPORT = 1337
private const val REQUEST_IMPORT = 1338

class MainActivity : AppCompatActivity() {
  private val vm: MainViewModel by viewModel()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    vm.viewStates.observe(this) { state ->
      when (state) {
        MainViewState.Loading -> {
          binding.progress.isVisible = true
          binding.content.isVisible = false
          binding.state.isVisible = false
        }
        is MainViewState.Content -> {
          binding.progress.isVisible = false
          binding.content.isVisible = true
          binding.state.isVisible = true
          updateContent(state.summary, binding)
        }
        MainViewState.Error -> {
          binding.progress.isVisible = false
          binding.content.isVisible = false
          binding.state.isVisible = true
          binding.state.setText(R.string.error)
        }
      }
    }

    binding.populate.setOnClickListener { vm.populate() }
    binding.exportDatabase.setOnClickListener { openForExport() }
    binding.importDatabase.setOnClickListener { openForImport() }
  }

  override fun onActivityResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    when (requestCode) {
      REQUEST_EXPORT -> {
        if (resultCode == Activity.RESULT_OK) {
          data?.data?.let { uri -> vm.export(uri) }
        }
      }
      REQUEST_IMPORT -> {
        if (resultCode == Activity.RESULT_OK) {
          data?.data?.let { uri -> vm.import(uri) }
        }
      }
      else -> super.onActivityResult(requestCode, resultCode, data)
    }
  }

  private fun updateContent(summary: Summary, binding: ActivityMainBinding) {
    if (summary.count > 0) {
      val oldest = DATE_TIME_FORMATTER.format(summary.oldestTimestamp)

      binding.state.text =
        getString(R.string.summaryTemplate, summary.count, oldest)
      binding.exportDatabase.isEnabled = true
    } else {
      binding.state.setText(R.string.empty)
      binding.exportDatabase.isEnabled = false
    }
  }

  private fun openForExport() {
    try {
      val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
        .addCategory(Intent.CATEGORY_OPENABLE)
        .setType("application/octet-stream")
        .putExtra(Intent.EXTRA_TITLE, DEFAULT_EXPORT_TITLE)

      startActivityForResult(intent, REQUEST_EXPORT)
    } catch (t: Throwable) {
      Log.e(TAG, "Exception obtaining document for export", t)
      Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show()
    }
  }

  private fun openForImport() {
    try {
      val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        .addCategory(Intent.CATEGORY_OPENABLE)
        .setType("application/octet-stream")

      startActivityForResult(intent, REQUEST_IMPORT)
    } catch (t: Throwable) {
      Log.e(TAG, "Exception obtaining document for import", t)
      Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show()
    }
  }
}
