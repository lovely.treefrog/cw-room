/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.todo.repo

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class ToDoRepository(private val store: ToDoEntity.Store) {
  fun items(): Observable<List<ToDoModel>> = store.all()
    .map { all -> all.map { it.toModel() } }

  fun find(id: String): Maybe<ToDoModel> = store.find(id)
    .map { it.toModel() }
    .subscribeOn(Schedulers.io())

  fun save(model: ToDoModel) = store.save(ToDoEntity(model))
    .subscribeOn(Schedulers.io())

  fun delete(model: ToDoModel) = store.delete(ToDoEntity(model))
    .subscribeOn(Schedulers.io())
}