/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.todo.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class ToDoRepository(
  private val store: ToDoEntity.Store,
  private val executor: Executor = Executors.newSingleThreadExecutor()
) {
  fun items(): LiveData<List<ToDoModel>> = store.all().map { all ->
    all.map { it.toModel() }
  }

  fun find(id: String): LiveData<ToDoModel?> =
    store.find(id).map { it?.toModel() }

  fun save(model: ToDoModel) {
    executor.execute { store.save(ToDoEntity(model)) }
  }

  fun delete(model: ToDoModel) {
    executor.execute { store.delete(ToDoEntity(model)) }
  }
}