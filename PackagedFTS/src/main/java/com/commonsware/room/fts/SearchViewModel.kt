/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.fts

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData

class SearchViewModel(
  search: String,
  repo: BookRepository,
  private val context: Context
) : ViewModel() {
  val paragraphs: LiveData<List<String>> = liveData {
    try {
      emit(repo.filtered(search))
    } catch (t: Throwable) {
      Log.e("FTS", "Exception with search expression", t)
      emit(listOf(context.getString(R.string.search_error, search)))
    }
  }
}